`define ScreenH 10'd480
`define TurretHalf 10'd20
`define TurretSize 10'd40

// moving from 2cm to 46cm
module TurretDisplayController(
    input clk,
    input reset,
    input [10 - 1:0] h_cnt,
    input [10 - 1:0] v_cnt,
    input [10 - 1:0] distance,
    output valid,
    output [12 - 1:0] address
);

    wire [10 - 1:0] position_x;
    wire [10 - 1:0] position_y;

    assign position_x = `TurretHalf;
    assign position_y = 10'd480 - distance;

    assign valid = ((v_cnt >= position_y - `TurretHalf) && (v_cnt < position_y + `TurretHalf)) && ((h_cnt >= position_x - `TurretHalf) && (h_cnt < position_x + `TurretHalf));
    assign address = ((v_cnt - position_y + `TurretHalf) * `TurretSize + (h_cnt - position_x + `TurretHalf));
endmodule
