
module VGAController(
    input clk,
    input reset,
    output hsync, vsync, valid,
    output [10-1:0] h_cnt, v_cnt
);
    reg [10-1:0] pixel_cnt;
    reg [10-1:0] line_cnt;
    reg hsync_i, vsync_i;

    localparam HD = 640, VD = 480;
    localparam HF = 16, VF = 10;
    localparam HS = 96, VS = 2;
    localparam HB = 48, VB = 33;
    localparam HT = 800, VT = 525;
    localparam hsync_default = 1'b1;
    localparam vsync_default = 1'b1;

    always @(posedge clk) begin
        if (reset) begin
            pixel_cnt <= 10'b0;
        end else begin
            if (pixel_cnt < (HT - 1))
                pixel_cnt <= pixel_cnt + 1;
            else
                pixel_cnt <= 10'b0;
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            hsync_i <= hsync_default;
        end else begin
            if (pixel_cnt >= (HD + HF - 1) && pixel_cnt < (HD + HF + HS - 1))
                hsync_i <= ~hsync_default;
            else
                hsync_i <= hsync_default;
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            line_cnt <= 10'b0;
        end else begin
            if (pixel_cnt == (HT - 1)) begin
                if (line_cnt < (VT - 1))
                    line_cnt <= line_cnt + 1;
                else
                    line_cnt <= 10'b0;
            end else begin
                line_cnt <= line_cnt;
            end
        end
    end

    always @(posedge clk) begin
        if (reset) begin
            vsync_i <= vsync_default;
        end else begin
            if (line_cnt >= (VD + VF - 1) && line_cnt < (VD + VF + VS - 1))
                vsync_i <= ~vsync_default;
            else
                vsync_i <= vsync_default;
        end
    end

    assign hsync = hsync_i;
    assign vsync = vsync_i;
    assign valid = (pixel_cnt < HD && line_cnt < VD);
    assign h_cnt = (pixel_cnt < HD) ? pixel_cnt : 10'b0;
    assign v_cnt = (line_cnt < VD) ? line_cnt : 10'b0;
endmodule

`define true 1'b1
`define false 1'b0

module VGADisplay(
    input clk,
    input reset,
    input play,
    input cheat,
    input [10 - 1:0] distance,
    output reg [6 - 1:0] score,
    output hsync,
    output vsync,
    output [4 - 1:0] vgaRed,
    output [4 - 1:0] vgaGreen,
    output [4 - 1:0] vgaBlue
);
    wire d_clk2;
    ClockDivider_25MHz clock_divider_25Mhz_inst(
        .clk(clk), 
        .reset(reset), 
        .d_clk(d_clk2)
    );

    wire [10 - 1:0] h_cnt, v_cnt;
    wire valid;
    VGAController vga_controller_inst(
        .clk(d_clk2),
        .reset(reset),
        .hsync(hsync),
        .vsync(vsync),
        .valid(valid),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt)
    );

    wire [12 - 1:0] start_scene_pixel;
    StartScene start_scene_inst(
        .clk(clk),
        .reset(reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .start_scene_pixel(start_scene_pixel)
    );

    wire [12 - 1:0] play_scene_pixel;
    wire game_over;
    wire [6 - 1:0] enemy_died_counter;
    PlayScene play_scene_inst(
        .clk(clk),
        .reset(play || reset),
        .random_reset(reset),
        .distance(distance),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .play_scene_pixel(play_scene_pixel),
        .game_over(game_over),
        .enemy_died_counter(enemy_died_counter)
    );

    reg [2 - 1:0] state, next_state;
    reg [12 - 1:0] color, next_color;
    reg [6 - 1:0] next_score;

    localparam START = 2'b00;
    localparam PLAY = 2'b01;
    localparam LOSE = 2'b10;
    localparam WIN = 2'b11;

    always @(posedge clk) begin
        if (reset) begin
            state <= START;
            color <= 12'h0;
            score <= 6'b0;
        end else begin
            state <= next_state;
            color <= next_color;
            score <= (play) ? 6'b0 : next_score;
        end
    end

    // TODO: LoseScene, WinScene
    always @(*) begin
        case(state)
            START: begin
                next_state = (play) ? PLAY : START;
                next_color = start_scene_pixel;
                next_score = 6'b0;
            end
            PLAY: begin
                if (game_over) begin
                    next_state = LOSE;
                end else begin
                    if (score == 6'd15 || cheat)
                        next_state = WIN;
                    else
                        next_state = PLAY;
                end
                next_score = enemy_died_counter;
                next_color = play_scene_pixel;
            end
            LOSE: begin
                next_state = (play) ? PLAY : LOSE;
                next_color = 12'h508;
                next_score = score;
            end
            WIN: begin
                next_state = (play) ? PLAY : WIN;
                next_color = 12'hf80;
                next_score = score;
            end
        endcase
    end

    assign {vgaRed, vgaGreen, vgaBlue} = (valid == 1'b1) ? color : 12'h0;
endmodule
