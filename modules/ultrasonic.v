`timescale 1ns/1ps

// VCC, GND: control power
// Mode of operation:
//  1. give the VCC, GND signal (power)
//  2. give 'trig' pin a signal which > 10us
//  3. it will automatically launch 8 40Khz sound wave, simultaniously, the 'echo' pin will turn from 0 to 1
//  4. when it received the sound wave(come back), 'echo' pin will turn from 1 to 0, we can use stopwatch to record the time
//  5. sound speed: 344 m/s, and we have time T = 2t, so the distance = 344 * T / 2

// produce 10Hz signal
module GenerateTrigger(
    input clk,
    input reset,
    output trig
);
    reg [27 - 1:0] counter;
    wire [27 - 1:0] next_counter;

    assign next_counter = counter + 1'b1;
    assign trig = (counter < 27'd999) ? 1'b1 : 1'b0;

    always @(posedge clk) begin
        if (reset) begin
            counter <= 27'b0;
        end else begin
            counter <= (counter == 27'd100000000) ? 24'b0 : next_counter;
        end
    end
endmodule

module CountDistance(
    input clk,
    input reset,
    input echo,
    output reg [10 - 1:0] distance // 1 unit 1 mm (0.1cm)
);

    reg [2 - 1:0] state, next_state;
    reg [20 - 1:0] counter, next_counter;
    reg [20 - 1:0] dist, next_dist;

    localparam IDLE = 2'b00;
    localparam CNT = 2'b01;
    localparam FINISH = 2'b10;

    always @(posedge clk) begin
        if (reset) begin
            state <= IDLE;
            counter <= 20'b0;
            dist <= 20'b0;
        end else begin
            state <= next_state;
            counter <= next_counter;
            dist <= next_dist;
        end
    end

    always @(*) begin
        next_dist = dist;
        case(state)
            IDLE: begin
                next_state = (echo == 1'b1) ? CNT : IDLE;
                next_counter = 20'b0;
            end
            CNT: begin
                next_state = (echo == 1'b0) ? FINISH : CNT;
                next_counter = counter + 1'b1;
            end
            FINISH: begin
                next_state = IDLE;
                next_counter = 20'b0;
                next_dist = counter / 20'd578;
            end
            default: begin
                next_counter = counter;
                next_state = state;
                next_dist = dist;
            end
        endcase
    end

    localparam MinDist = 20'd20;
    localparam MaxDist = 20'd460;

    always @(*) begin
        if (dist >= MinDist && dist <= MaxDist) begin
            distance = dist[9:0];
        end else begin
            if (dist > MaxDist)
                distance = MaxDist;
            else
                distance = MinDist;
        end
    end
endmodule
