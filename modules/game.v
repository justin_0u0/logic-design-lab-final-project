
module GameController(
    input clk,
    input btnC,
    input btnU,
    input sw0, // switch 0 
    input echo, // A17
    output trig, // A15
    output [4 - 1:0] an,
    output [7 - 1:0] seg,
    output dp,
    output hsync,
    output vsync,
    output [4 - 1:0] vgaRed,
    output [4 - 1:0] vgaGreen,
    output [4 - 1:0] vgaBlue,
    output [2 - 1:0] led
);

    wire btnC_d, reset;
    wire btnU_d, play;
    wire [10 - 1:0] distance;
    wire [6 - 1:0] score;

    Debounce debounce_inst_0(clk, btnC, btnC_d);
    OnePulse onepulse_inst_0(clk, btnC_d, reset);
    Debounce debounce_inst_1(clk, btnU, btnU_d);
    OnePulse onepulse_inst_1(clk, btnU_d, play);

    assign led[0] = echo;
    assign led[1] = (distance < 10'd100) ? 1'b1 : 1'b0;

    GenerateTrigger generate_trigger_inst_0(
        .clk(clk), 
        .reset(reset), 
        .trig(trig)
    );

    CountDistance count_distance_inst(
        .clk(clk),
        .reset(reset),
        .echo(echo),
        .distance(distance)
    );

    SevenSegment seven_segment_inst(
        .clk(clk),
        .reset(reset),
        .values(score),
        .an(an),
        .seg(seg),
        .dp(dp)
    );

    VGADisplay vga_display_inst_0(
        .clk(clk),
        .reset(reset),
        .play(play),
        .cheat(sw0),
        .distance(distance),
        .score(score),
        .hsync(hsync),
        .vsync(vsync),
        .vgaRed(vgaRed),
        .vgaGreen(vgaGreen),
        .vgaBlue(vgaBlue)
    );
endmodule

