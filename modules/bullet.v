`define false 1'b0
`define true 1'b1

// when valid is ture, we'll have a bullet
// bullet speed = 1 pixel/0.01s
module Bullet(
    input clk,
    input reset,
    input valid,
    input [10 - 1:0] distance,
    input hit,
    output reg exist,
    output reg [10 - 1:0] position_x,
    output reg [10 - 1:0] position_y
);
    wire d_clk;
    ClockDivider #(20) clock_divider_inst_bullet_1(
        .clk(clk),
        .reset(reset),
        .d_clk(d_clk)
    );

    localparam WAIT = 2'b00;
    localparam READY = 2'b01;
    localparam SHOOT = 2'b10;

    reg [2 - 1:0] state, next_state;
    reg next_exist;
    reg [10 - 1:0] next_position_x;
    reg [10 - 1:0] next_position_y;

    always @(posedge clk) begin
        if (reset) begin
            state <= WAIT;
            exist <= `false;
            position_x <= 10'b0;
            position_y <= 10'b0;
        end else begin
            state <= next_state;
            position_y <= next_position_y;
            position_x <= next_position_x;
            exist <= next_exist;
        end
    end

    localparam ScreenW = 10'd640;
    localparam ScreenH = 10'd480;
    localparam StartX = 10'd55;
    wire speed;
    assign speed = 10'b1; // about 0.01s go 1 pixel

    always @(*) begin
        case(state)
            WAIT: begin
                next_position_x = position_x;
                next_position_y = position_y;
                next_exist = `false;
                next_state = (valid == `true) ? READY : WAIT;
            end
            READY: begin
                next_position_x = StartX;
                next_position_y = 10'd480 - distance;
                next_exist = `true;
                next_state = SHOOT;
            end
            SHOOT: begin
                next_position_x = (d_clk) ? position_x + speed : position_x;
                next_position_y = position_y;
                next_exist = `true;
                next_state = (position_x == ScreenW || hit == `true) ? WAIT : SHOOT;
            end
            default: begin
                next_position_x = position_x;
                next_position_y = position_y;
                next_exist = exist;
                next_state = state;
            end
        endcase
    end
endmodule

`define MaxBullet 6

module BulletsController(
    input clk, 
    input reset,
    input [10 - 1:0] distance,
    input [`MaxBullet - 1:0] hits,
    output [`MaxBullet - 1:0] exists,
    output [`MaxBullet * 10 - 1:0] positions_x,
    output [`MaxBullet * 10 - 1:0] positions_y
);

    wire d_clk;
    ClockDivider #(27) clock_divider_inst_generate_bullet_0(
        .clk(clk),
        .reset(reset),
        .d_clk(d_clk)
    );

    // about 0.35sec 1 bullet
    // get the rightest 0
    wire [`MaxBullet - 1:0] valid;
    assign valid = (d_clk) ? ((~exists) & (exists + 1'b1)) : {`MaxBullet{1'b0}};

    Bullet bullet_inst_0(
        .clk(clk),
        .reset(reset),
        .valid(valid[0]),
        .distance(distance),
        .hit(hits[0]),
        .exist(exists[0]),
        .position_x(positions_x[10 - 1:0]),
        .position_y(positions_y[10 - 1:0])
    );
    Bullet bullet_inst_1(
        .clk(clk),
        .reset(reset),
        .valid(valid[1]),
        .distance(distance),
        .hit(hits[1]),
        .exist(exists[1]),
        .position_x(positions_x[20 - 1:10]),
        .position_y(positions_y[20 - 1:10])
    );
    Bullet bullet_inst_2(
        .clk(clk),
        .reset(reset),
        .valid(valid[2]),
        .distance(distance),
        .hit(hits[2]),
        .exist(exists[2]),
        .position_x(positions_x[30 - 1:20]),
        .position_y(positions_y[30 - 1:20])
    );
    Bullet bullet_inst_3(
        .clk(clk),
        .reset(reset),
        .valid(valid[3]),
        .distance(distance),
        .hit(hits[3]),
        .exist(exists[3]),
        .position_x(positions_x[40 - 1:30]),
        .position_y(positions_y[40 - 1:30])
    );
    Bullet bullet_inst_4(
        .clk(clk),
        .reset(reset),
        .valid(valid[4]),
        .distance(distance),
        .hit(hits[4]),
        .exist(exists[4]),
        .position_x(positions_x[50 - 1:40]),
        .position_y(positions_y[50 - 1:40])
    );
    Bullet bullet_inst_5(
        .clk(clk),
        .reset(reset),
        .valid(valid[5]),
        .distance(distance),
        .hit(hits[5]),
        .exist(exists[5]),
        .position_x(positions_x[60 - 1:50]),
        .position_y(positions_y[60 - 1:50])
    );
endmodule

module BulletsDisplayController(
    input clk,
    input reset,
    input [10 - 1:0] h_cnt,
    input [10 - 1:0] v_cnt,
    input [10 - 1:0] distance,
    input [`MaxBullet - 1:0] hits,
    output exist,
    output [7 - 1:0] address,
    output [`MaxBullet - 1:0] exists,
    output [`MaxBullet * 10 - 1:0] positions_x,
    output [`MaxBullet * 10 - 1:0] positions_y
);
    wire [`MaxBullet - 1:0] bullets;
    wire [10 - 1:0] bullets_x [0:`MaxBullet - 1];
    wire [10 - 1:0] bullets_y [0:`MaxBullet - 1];

    BulletsController bullets_controller_inst(
        .clk(clk),
        .reset(reset),
        .distance(distance),
        .hits(hits),
        .exists(exists),
        .positions_x(positions_x),
        .positions_y(positions_y)
    );

    assign bullets = exists;
    assign {
        bullets_x[5], bullets_x[4], bullets_x[3], bullets_x[2], bullets_x[1], bullets_x[0]
    } = positions_x;
    assign {
        bullets_y[5], bullets_y[4], bullets_y[3], bullets_y[2], bullets_y[1], bullets_y[0]
    } = positions_y;

    localparam BulletHalf = 10'd5;
    localparam BulletSize = 10'd10;

    wire [`MaxBullet - 1:0] is_bullets;
    // is_bullets: the display point is in range of the bullets
	PointInsideRectangle point_inside_rectangle_inst_bullet_0(
		.valid(bullets[0]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(bullets_x[0]),
		.rectangle_y(bullets_y[0]),
		.half_rectangle_size(BulletHalf),
		.inside(is_bullets[0])
	);
	PointInsideRectangle point_inside_rectangle_inst_bullet_1(
		.valid(bullets[1]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(bullets_x[1]),
		.rectangle_y(bullets_y[1]),
		.half_rectangle_size(BulletHalf),
		.inside(is_bullets[1])
	);
	PointInsideRectangle point_inside_rectangle_inst_bullet_2(
		.valid(bullets[2]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(bullets_x[2]),
		.rectangle_y(bullets_y[2]),
		.half_rectangle_size(BulletHalf),
		.inside(is_bullets[2])
	);
	PointInsideRectangle point_inside_rectangle_inst_bullet_3(
		.valid(bullets[3]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(bullets_x[3]),
		.rectangle_y(bullets_y[3]),
		.half_rectangle_size(BulletHalf),
		.inside(is_bullets[3])
	);
	PointInsideRectangle point_inside_rectangle_inst_bullet_4(
		.valid(bullets[4]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(bullets_x[4]),
		.rectangle_y(bullets_y[4]),
		.half_rectangle_size(BulletHalf),
		.inside(is_bullets[4])
	);
	PointInsideRectangle point_inside_rectangle_inst_bullet_5(
		.valid(bullets[5]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(bullets_x[5]),
		.rectangle_y(bullets_y[5]),
		.half_rectangle_size(BulletHalf),
		.inside(is_bullets[5])
	);

    /*
     * We need one-hot-signal to find the displayed bullet  
     * So we find the rightmost 1
    */
    wire [`MaxBullet - 1:0] display_bullet;
    assign display_bullet = (is_bullets & (~is_bullets + 1'b1));

    reg [4 - 1:0] target;
    always @(*) begin
        case(display_bullet)
            `MaxBullet'b000001: target = 4'd0;
            `MaxBullet'b000010: target = 4'd1;
            `MaxBullet'b000100: target = 4'd2;
            `MaxBullet'b001000: target = 4'd3;
            `MaxBullet'b010000: target = 4'd4;
            `MaxBullet'b100000: target = 4'd5;
            default: target = `MaxBullet;
        endcase
    end

    assign exist = (target != `MaxBullet) ? `true : `false;
    assign address = (target != `MaxBullet) ? ((v_cnt - bullets_y[target] + BulletHalf) * BulletSize + (h_cnt - bullets_x[target] + BulletHalf)): 12'b0;
endmodule
