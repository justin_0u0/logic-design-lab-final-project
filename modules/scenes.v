module StartScene(
    input clk,
    input reset,
    input [10 - 1:0] h_cnt,
    input [10 - 1:0] v_cnt,
    output [12 - 1:0] start_scene_pixel
);
    
    wire [11:0] data;
    wire [17 - 1:0] start_scene_address;
    BlockMemory block_memory_inst_start_scene(
        .clka(clk),
        .wea(0),
        .addra(start_scene_address),
        .dina(start_scene_data),
        .douta(start_scene_pixel)
    );

    assign start_scene_address = ((h_cnt >> 1) + 320 * (v_cnt >> 1)) % 76800;
    // assign start_scene_pixel = 12'hFA0;
endmodule

`define MaxEnemy 5
`define MaxBullet 6

module PlayScene(
    input clk,
    input reset,
    input random_reset,
    input [10 - 1:0] distance, // for turret position
    input [10 - 1:0] h_cnt,
    input [10 - 1:0] v_cnt,
    output reg [12 - 1:0] play_scene_pixel,
    output game_over, // enemy reach the wall -> lose
    output [6 - 1:0] score // died_counter >= 100 -> win
);

    // Generate Address
    wire [12 - 1:0] turret_address;
    wire [12 - 1:0] enemy_address;
    wire [7 - 1:0] bullet_address;
    wire [12 - 1:0] turret_pixel;
    wire [12 - 1:0] type1_enemy_pixel;
    wire [12 - 1:0] type2_enemy_pixel;
    wire [12 - 1:0] bullet_pixel;
    TransferAddressToPixel transfer_address_to_pixel_inst(
        .clk(clk),
        .turret_address(turret_address),
        .enemy_address(enemy_address),
        .bullet_address(bullet_address),
        .turret_pixel(turret_pixel),
        .type1_enemy_pixel(type1_enemy_pixel),
        .type2_enemy_pixel(type2_enemy_pixel),
        .bullet_pixel(bullet_pixel)
    );

    wire [`MaxEnemy - 1:0] enemies_alive;
    wire [`MaxEnemy * 10 - 1:0] enemies_x;
    wire [`MaxEnemy * 10 - 1:0] enemies_y;
    wire [`MaxEnemy - 1:0] enemies_hit;
    wire [`MaxBullet -  1:0] bullets_exist;
    wire [`MaxBullet * 10 - 1:0] bullets_x;
    wire [`MaxBullet * 10 - 1:0] bullets_y;
    wire [`MaxBullet - 1:0] bullets_hit;
    // Handle Collision
    EnemiesBulletsCollision enemies_bullets_collision_inst(
        .enemies_valid(enemies_alive),
        .enemies_x(enemies_x),
        .enemies_y(enemies_y),
        .bullets_valid(bullets_exist),
        .bullets_x(bullets_x),
        .bullets_y(bullets_y),
        .enemies_hit(enemies_hit), // tell us which enemy is hit
        .bullets_hit(bullets_hit) // tell us which bullet hit the enemy
    );
    
    EnemiesWallCollision enemies_wall_collision_inst(
        .enemies_valid(enemies_alive),
        .enemies_x(enemies_x),
        .collide(game_over)
    );

    // VGA
    localparam VHalf = 10'd240;

    // bullets
    wire is_bullet;
    BulletsDisplayController bullets_display_controller_inst(
        .clk(clk),
        .reset(reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .distance(distance),
        .hits(bullets_hit),
        .exist(is_bullet), // whether bullet should show on the screen at (h_cnt, v_cnt) now or not
        .address(bullet_address), // for reading the image address
        .exists(bullets_exist), // tell us which bullet exist
        .positions_x(bullets_x),
        .positions_y(bullets_y)
    );

    // enemies
    wire is_enemy;
    wire enemy_type;
    EnemiesDisplayController enemies_display_controller_inst(
        .clk(clk),
        .reset(reset),
        .random_reset(random_reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .hits(enemies_hit),
        .damage(5'd1),
        .valid(is_enemy),
        .type(enemy_type),
        .address(enemy_address),
        .alives(enemies_alive),
        .positions_x(enemies_x),
        .positions_y(enemies_y),
        .died_counter(score)
    );
    wire [12 - 1:0] enemy_pixel;
    assign enemy_pixel = (enemy_type == 1'b0) ? type1_enemy_pixel : type2_enemy_pixel;

    // turret (upper-left = (220, 20))
    localparam TurretX = 10'd0;
    localparam TurretHalf = 10'd20;
    localparam TurretSize = 10'd40;
    wire is_turret;
    TurretDisplayController turret_display_controller_inst(
        .clk(clk),
        .reset(reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .distance(distance),
        .valid(is_turret),
        .address(turret_address)
    );

    // wall
    localparam WallColor = 12'h630;
    localparam WallW = 10'd16;
    localparam WallX = 10'd40;
    wire is_wall;
    assign is_wall = (h_cnt >= WallX) && (h_cnt <= WallX + WallW);

    // territory
    localparam Territory = 12'h000;
    wire is_territory;
    assign is_territory = (h_cnt < WallX); 

    // background
    localparam Background = 12'h059; // rgb(0, 80, 144)
    wire is_background;
    assign is_background = (v_cnt >= 10'd0 && v_cnt <= 10'd480) && (h_cnt >= 10'd0 && h_cnt <= 10'd640);

    // Summary: Display
    `define Layers 6
    wire [`Layers - 1:0] display;
    assign display = {
        is_bullet,
        is_enemy,
        is_turret,
        is_wall,
        is_territory,
        is_background //1'b1 // background
    };

    always @(*) begin
        casex (display)
            `Layers'b1xxxxx: play_scene_pixel = bullet_pixel;
            `Layers'b01xxxx: play_scene_pixel = enemy_pixel;
            `Layers'b001xxx: play_scene_pixel = turret_pixel;
            `Layers'b0001xx: play_scene_pixel = WallColor;
            `Layers'b00001x: play_scene_pixel = Territory;
            `Layers'b000001: play_scene_pixel = Background;
            default: play_scene_pixel = 12'h000;
        endcase
    end
endmodule
