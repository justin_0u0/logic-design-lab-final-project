
module SevenSegment(
    input clk,
    input reset,
    input [6 - 1:0] values,
    output reg [4 - 1:0] an,
    output reg [7 - 1:0] seg,
    output dp
);
    parameter S1 = 2'b00;
    parameter S2 = 2'b01;
    parameter S3 = 2'b10;
    parameter S4 = 2'b11;

    reg [2 - 1:0] state, next_state;
    reg [4 - 1:0] next_an;
    reg [4 - 1:0] number;
    wire dclk;

    ClockDivider #(.N(13)) clock_divider_inst_7segment(
        .clk(clk),
        .reset(reset),
        .d_clk(dclk)
    );

    assign dp = 1'b1;

    always @(posedge clk) begin
        if (reset) begin
            state <= S1;
        end else begin
            if (dclk) begin
                state <= next_state;
            end else begin
                state <= state;
            end
        end
    end
    
    always @(posedge clk) begin
        case(state)
            S1: begin
                an <= 4'b1110;
                number <= values % 10'd10;
                next_state <= S2;
            end
            S2: begin
                an <= 4'b1101;
                number <= (values / 10'd10);
                next_state <= S1;
            end
            // S3: begin
            //     an <= 4'b1011;
            //     number <= (values / 10'd100) % 10'd10;
            //     next_state <= S4;
            // end
            // S4: begin
            //     an <= 4'b0111;
            //     number <= (values / 10'd1000) % 10'd10;
            //     next_state <= S1;
            // end
            default: begin
                an <= an;
                number <= number;
                next_state <= state;
            end
        endcase
    end

    always @(number) begin
        case(number)
            4'd0: seg = 7'b1000000;
            4'd1: seg = 7'b1111001;
            4'd2: seg = 7'b0100100;
            4'd3: seg = 7'b0110000;
            4'd4: seg = 7'b0011001;
            4'd5: seg = 7'b0010010;
            4'd6: seg = 7'b0000010;
            4'd7: seg = 7'b1111000;
            4'd8: seg = 7'b0000000;
            4'd9: seg = 7'b0010000;
            default: seg = 7'b0111111; // -
        endcase
    end
endmodule
