
`define true 1'b1
`define false 1'b0

module GenerateEnemy(
    input clk,
    input reset,
    output spawn,
    output spawn_type,
    output [9 - 1:0] spawn_y
);
    wire d_clk;
    ClockDivider #(25) clock_divider_inst_enemy_0(
        .clk(clk),
        .reset(reset),
        .d_clk(d_clk)
    );

    wire [3 - 1:0] lfsr;
    RandomInteger #(3) random_number_inst_enemy_0(
        .clk(clk),
        .reset(reset),
        .lfsr(lfsr)
    );
    // interval = 12 * (2 ** 25) ~ 18 * (2 ** 25), approximately 4 sec to 6.3 sec
    reg [5 - 1:0] interval, next_interval;

    wire [9 - 1:0] lfsr2;
    RandomInteger #(9) random_number_inst_enemy_1(
        .clk(clk),
        .reset(reset),
        .lfsr(lfsr2)
    );
    assign spawn_y = lfsr2 % 440 + 20; // spawn_y = random number [20, 440]

    // Enemy Type
    wire [2 - 1:0] lfsr3;
    RandomInteger #(2) random_number_inst_enemy_2(
        .clk(clk),
        .reset(reset),
        .lfsr(lfsr3)
    );
    assign spawn_type = lfsr3[0];

    localparam PREPARE = 2'b00; // get new interval
    localparam WAIT = 2'b01; // wait until interval reach zero
    localparam GENERATE = 2'b10; // generate enemy
    reg [2 - 1:0] state, next_state;

    always @(posedge clk) begin
        if (reset) begin
            state <= PREPARE;
            interval <= 1'b0;
        end else begin
            if (d_clk) begin
                state <= next_state;
                interval <= next_interval;
            end else begin
                if (state == GENERATE)
                    state <= next_state;
                else
                    state <= state;
                interval <= interval;
            end
        end
    end

    always @(*) begin
        case (state)
            PREPARE: begin
                next_interval = 5'd11 + lfsr;
                next_state = WAIT;
            end
            WAIT: begin
                if (interval == 5'b0) begin
                    next_state = GENERATE;
                    next_interval = interval;
                end else begin
                    next_state = WAIT;
                    next_interval = interval - 1'b1;
                end
            end
            GENERATE: begin
                next_state = PREPARE;
                next_interval = interval;
            end
        endcase
    end

    assign spawn = (state == GENERATE) ? `true : `false;
endmodule

module Enemy(
    input clk,
    input reset,
    input hit,
    input [5 - 1:0] damage,
    input spawn,
    input spawn_type,
    input [9 - 1:0] spawn_y,
    output reg alive,
    output reg type,
    output reg [10 - 1:0] position_x,
    output reg [10 - 1:0] position_y,
    output reg died
);
    wire d_clk;
    ClockDivider #(23) clock_divider_inst_enemy_1(
        .clk(clk),
        .reset(reset),
        .d_clk(d_clk)
    );

    reg next_alive;
    reg next_type;
    reg [10 - 1:0] next_position_x;
    reg [10 - 1:0] next_position_y;
    reg [5 - 1:0] health, next_health;
    reg next_died;
    always @(posedge clk) begin
        if (reset) begin
            alive <= `false;
            type <= 1'b0;
            position_x <= 10'b0;
            position_y <= 10'b0;
            health <= 5'b0;
            died <= `false;
        end else begin
            alive <= next_alive;
            type <= next_type;
            position_x <= next_position_x;
            position_y <= next_position_y;
            health <= next_health;
            died <= next_died;
        end
    end

    localparam ScreenW = 640;
    localparam WallX = 75;
    wire [10 - 1:0] speed;
    assign speed = (type == 1'b0) ? 10'd4 : 10'd5;

    always @(*) begin
        if (alive == `true) begin
            // alive & health & died
            if (hit == `true) begin
                if (health <= damage) begin
                    next_alive = `false;
                    next_health = 5'b0;
                    next_died = `true;
                end else begin
                    next_alive = alive;
                    next_health = health - damage;
                    next_died = `false;
                end
            end else begin
                next_alive = alive;
                next_health = health;
                next_died = `false;
            end
            // type
            next_type = type;
            // position_x
            if (d_clk) begin
                if (position_x - speed >= WallX)
                    next_position_x = position_x - speed;
                else
                    next_position_x = WallX;
            end else begin
                next_position_x = position_x;
            end
            // position_y
            next_position_y = position_y;
        end else begin
            if (spawn == `true) begin
                next_alive = `true;
                next_type = spawn_type;
                next_position_x = ScreenW;
                next_position_y = spawn_y;
				next_health = 5'd2;
            end else begin
                next_alive = alive;
                next_type = type;
                next_position_x = position_x;
                next_position_y = position_y;
				next_health = health;
            end
			next_died = `false;
        end
    end
endmodule

`define MaxEnemy 5

// For convinient, we assume that no enemy will died in the same cycle
module EnemyDiedCounter(
    input clk,
    input reset,
    input died,
    output [6 - 1:0] result
);
    reg [6 - 1:0] counter, next_counter;

    always @(posedge clk) begin
        if (reset) begin
            counter <= 6'b0;
        end else begin
            if (died == `true)
                counter <= counter + 1'b1;
            else
                counter <= counter;
        end
    end
    assign result = counter;
endmodule

module EnemyController(
    input clk,
    input reset,
    input random_reset,
    input [`MaxEnemy - 1:0] hits,
    input [5 - 1:0] damage,
    output [`MaxEnemy - 1:0] alives,
    output [`MaxEnemy - 1:0] types,
    output [`MaxEnemy * 10 - 1:0] positions_x,
    output [`MaxEnemy * 10 - 1:0] positions_y,
    output [6 - 1:0] died_counter
);
    wire spawn;
    wire spawn_type;
    wire [9 - 1:0] spawn_y;
    GenerateEnemy generate_enemy_inst(
        .clk(clk),
        .reset(random_reset),
        .spawn(spawn),
        .spawn_type(spawn_type),
        .spawn_y(spawn_y)
    );

    /*
     * To find the which enemy is not alive,
     * so we can spawn the new one,
     * that is, to get the rightmost unset bit of 'alive' signal
     * that is, (~alive) & (alive + 1)
     */
    wire [`MaxEnemy - 1:0] spawns;
    assign spawns = (spawn == `true) ? ((~alives) & (alives + 1'b1)) : {`MaxEnemy{1'b0}};

    wire [`MaxEnemy - 1:0] dieds;

    // Connect singals
	Enemy enemy_inst_0(
		.clk(clk),
		.reset(reset),
		.hit(hits[0]),
		.damage(damage),
		.spawn(spawns[0]),
		.spawn_type(spawn_type),
		.spawn_y(spawn_y),
		.alive(alives[0]),
		.type(types[0]),
		.position_x(positions_x[10 - 1:0]),
		.position_y(positions_y[10 - 1:0]),
		.died(dieds[0])
	);
	Enemy enemy_inst_1(
		.clk(clk),
		.reset(reset),
		.hit(hits[1]),
		.damage(damage),
		.spawn(spawns[1]),
		.spawn_type(spawn_type),
		.spawn_y(spawn_y),
		.alive(alives[1]),
		.type(types[1]),
		.position_x(positions_x[20 - 1:10]),
		.position_y(positions_y[20 - 1:10]),
		.died(dieds[1])
	);
	Enemy enemy_inst_2(
		.clk(clk),
		.reset(reset),
		.hit(hits[2]),
		.damage(damage),
		.spawn(spawns[2]),
		.spawn_type(spawn_type),
		.spawn_y(spawn_y),
		.alive(alives[2]),
		.type(types[2]),
		.position_x(positions_x[30 - 1:20]),
		.position_y(positions_y[30 - 1:20]),
		.died(dieds[2])
	);
	Enemy enemy_inst_3(
		.clk(clk),
		.reset(reset),
		.hit(hits[3]),
		.damage(damage),
		.spawn(spawns[3]),
		.spawn_type(spawn_type),
		.spawn_y(spawn_y),
		.alive(alives[3]),
		.type(types[3]),
		.position_x(positions_x[40 - 1:30]),
		.position_y(positions_y[40 - 1:30]),
		.died(dieds[3])
	);
	Enemy enemy_inst_4(
		.clk(clk),
		.reset(reset),
		.hit(hits[4]),
		.damage(damage),
		.spawn(spawns[4]),
		.spawn_type(spawn_type),
		.spawn_y(spawn_y),
		.alive(alives[4]),
		.type(types[4]),
		.position_x(positions_x[50 - 1:40]),
		.position_y(positions_y[50 - 1:40]),
		.died(dieds[4])
	);

    EnemyDiedCounter enemy_died_counter_inst(
        .clk(clk),
        .reset(reset),
        .died((|dieds)),
        .result(died_counter)
    );
endmodule

module EnemiesDisplayController(
    input clk,
    input reset,
    input random_reset,
    input [10 - 1:0] h_cnt,
    input [10 - 1:0] v_cnt,
    input [`MaxEnemy - 1:0] hits,
    input [5 - 1:0] damage,
    output valid,
    output type,
    output [12 - 1:0] address,
    output [`MaxEnemy - 1:0] alives,
    output [`MaxEnemy * 10 - 1:0] positions_x,
    output [`MaxEnemy * 10 - 1:0] positions_y,
    output [6 - 1:0] died_counter
);
    wire [`MaxEnemy - 1:0] enemies;
    wire [`MaxEnemy - 1:0] enemies_type;
    wire [10 - 1:0] enemies_x [0:`MaxEnemy - 1];
    wire [10 - 1:0] enemies_y [0:`MaxEnemy - 1];
    EnemyController enemy_controller_inst(
        .clk(clk),
        .reset(reset),
        .random_reset(random_reset),
        .hits(hits),
        .damage(damage),
        .alives(alives),
        .types(enemies_type),
        .positions_x(positions_x),
        .positions_y(positions_y),
        .died_counter(died_counter)
    );

    // Destruct Signal
    assign enemies = alives;
    assign {
        enemies_x[4], enemies_x[3], enemies_x[2], enemies_x[1], enemies_x[0]
    } = positions_x;
    assign {
        enemies_y[4], enemies_y[3], enemies_y[2], enemies_y[1], enemies_y[0]
    } = positions_y;

    localparam EnemyHalf = 20;
    localparam EnemySize = 40;

    // is_enemies: whether the current vga display is in the range of that enemy
    wire [`MaxEnemy - 1:0] is_enemies;
	PointInsideRectangle point_inside_rectangle_inst_enemy_0(
		.valid(enemies[0]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(enemies_x[0]),
		.rectangle_y(enemies_y[0]),
		.half_rectangle_size(EnemyHalf),
		.inside(is_enemies[0])
	);
	PointInsideRectangle point_inside_rectangle_inst_enemy_1(
		.valid(enemies[1]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(enemies_x[1]),
		.rectangle_y(enemies_y[1]),
		.half_rectangle_size(EnemyHalf),
		.inside(is_enemies[1])
	);
	PointInsideRectangle point_inside_rectangle_inst_enemy_2(
		.valid(enemies[2]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(enemies_x[2]),
		.rectangle_y(enemies_y[2]),
		.half_rectangle_size(EnemyHalf),
		.inside(is_enemies[2])
	);
	PointInsideRectangle point_inside_rectangle_inst_enemy_3(
		.valid(enemies[3]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(enemies_x[3]),
		.rectangle_y(enemies_y[3]),
		.half_rectangle_size(EnemyHalf),
		.inside(is_enemies[3])
	);
	PointInsideRectangle point_inside_rectangle_inst_enemy_4(
		.valid(enemies[4]),
		.point_x(h_cnt),
		.point_y(v_cnt),
		.rectangle_x(enemies_x[4]),
		.rectangle_y(enemies_y[4]),
		.half_rectangle_size(EnemyHalf),
		.inside(is_enemies[4])
	);

    wire [`MaxEnemy - 1:0] displayed_enemy;
    assign displayed_enemy = ((is_enemies) & (~is_enemies + 1'b1));

    reg [4 - 1:0] target;
    always @(*) begin
        case (displayed_enemy)
            `MaxEnemy'b10000: target = 4'd4;
            `MaxEnemy'b01000: target = 4'd3;
            `MaxEnemy'b00100: target = 4'd2;
            `MaxEnemy'b00010: target = 4'd1;
            `MaxEnemy'b00001: target = 4'd0;
            default: target = `MaxEnemy;
        endcase
    end

    assign valid = (target == `MaxEnemy) ? `false : `true;
    assign type = enemies_type[target];
    assign address = (target == `MaxEnemy) ? 12'b0 : (v_cnt - enemies_y[target] + EnemyHalf) * EnemySize + (h_cnt - enemies_x[target] + EnemyHalf);
endmodule
