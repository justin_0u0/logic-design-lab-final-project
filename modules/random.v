
module RandomInteger(clk, reset, lfsr);
    parameter width = 8;
    input clk, reset;
    output reg [width - 1:0] lfsr;

    // taps of n bits lfsr
    
    wire [width - 1:0] tap_array [2:32];
    assign tap_array[2] = 2'b11;
    assign tap_array[3] = 3'b101;
    assign tap_array[4] = 4'b1001;
    assign tap_array[5] = 5'b10010;
    assign tap_array[6] = 6'b100001;
    assign tap_array[7] = 7'b1000001;
    assign tap_array[8] = 8'b10001110;
    assign tap_array[9] = 9'b100001000;
    assign tap_array[10] = 10'b1000000100;
    assign tap_array[11] = 11'b10000000010;
    assign tap_array[12] = 12'b100000101001;
    assign tap_array[13] = 13'b1000000001101;
    assign tap_array[14] = 14'b10000000010101;
    assign tap_array[15] = 15'b100000000000001;
    assign tap_array[16] = 16'b1000000000010110;
    assign tap_array[17] = 17'b10000000000000100;
    assign tap_array[18] = 18'b100000000001000000;
    assign tap_array[19] = 19'b1000000000000010011;
    assign tap_array[20] = 20'b10000000000000000100;
    assign tap_array[21] = 21'b100000000000000000010;
    assign tap_array[22] = 22'b1000000000000000000001;
    assign tap_array[23] = 23'b10000000000000000010000;
    assign tap_array[24] = 24'b100000000000000000001101;
    assign tap_array[25] = 25'b1000000000000000000000100;
    assign tap_array[26] = 26'b10000000000000000000100011;
    assign tap_array[27] = 27'b100000000000000000000010011;
    assign tap_array[28] = 28'b1000000000000000000000000100;
    assign tap_array[29] = 29'b10000000000000000000000000010;
    assign tap_array[30] = 30'b100000000000000000000000101001;
    assign tap_array[31] = 31'b1000000000000000000000000000100;
    assign tap_array[32] = 32'b10000000000000000000000001100010;
    
    
    wire [width - 1:0] taps;
    assign taps = tap_array[width];

    reg [6 - 1:0] state;
    reg [width - 1:0] next_lfsr;
    reg [width - 1:0] feedback, next_feedback;

    always @(posedge clk) begin
        if (reset) begin
            state <= width - 1'b1;
            lfsr <= 1'b1;
            feedback <= 1'b0;
        end else begin
            if (state == 6'd0)
                state <= width;
            else
                state <= state - 1'b1;
            lfsr <= next_lfsr;
            feedback <= next_feedback;
        end
    end

    always @(*) begin
        if (state == width) begin
            next_feedback = 1'b0;
            next_lfsr = (lfsr >> 1) | (feedback << (width - 1'b1));
        end else begin
            next_lfsr = lfsr;
            if (taps[state] == 1'b1)
                next_feedback = feedback ^ (lfsr >> (width - state - 1'b1));
            else
                next_feedback = feedback;
        end
    end
endmodule
