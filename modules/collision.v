
`define MaxBullet 6
`define MaxEnemy 5

`define true 1'b1
`define false 1'b0

/*
 * We assume all hitbox of rectangles are circles
 */
module CirclesCollision(
    input valid,
    input signed [10 - 1:0] circle1_x,
    input signed [10 - 1:0] circle1_y,
    input [10 - 1:0] circle1_radius,
    input signed [10 - 1:0] circle2_x,
    input signed [10 - 1:0] circle2_y,
    input [10 - 1:0] circle2_radius,
    output collide
);
    wire [20 - 1:0] distance_x, distance_y;
    assign distance_x = (circle1_x - circle2_x) * (circle1_x - circle2_x);
    assign distance_y = (circle1_y - circle2_y) * (circle1_y - circle2_y);

    wire [20 - 1:0] distance_radius;
    assign distance_radius = (circle1_radius + circle2_radius) * (circle1_radius + circle2_radius);

    assign collide = (valid == `true) && ((distance_x + distance_y <= distance_radius) == `true);
endmodule

/*
 * Handle One Enemy to Many Bullets Collision
 */
module EnemyBulletsCollision(
    input enemy_valid,
    input [10 - 1:0] enemy_x,
    input [10 - 1:0] enemy_y,
    input [`MaxBullet - 1:0] bullets_valid,
    input [`MaxBullet * 10 - 1:0] bullets_x,
    input [`MaxBullet * 10 - 1:0] bullets_y,
    output enemy_hit
);
    // declare constants
    localparam EnemyHalf = 10'd20;
    localparam BulletHalf = 10'd5;

    // connect signals
    wire [`MaxBullet - 1:0] enemy_hits;
	CirclesCollision circles_collision_inst_enemy_bullet_0(
		.valid(enemy_valid & bullets_valid[0]),
		.circle1_x(enemy_x),
		.circle1_y(enemy_y),
		.circle1_radius(EnemyHalf),
		.circle2_x(bullets_x[10 - 1:0]),
		.circle2_y(bullets_y[10 - 1:0]),
		.circle2_radius(BulletHalf),
		.collide(enemy_hits[0])
	);
	CirclesCollision circles_collision_inst_enemy_bullet_1(
		.valid(enemy_valid & bullets_valid[1]),
		.circle1_x(enemy_x),
		.circle1_y(enemy_y),
		.circle1_radius(EnemyHalf),
		.circle2_x(bullets_x[20 - 1:10]),
		.circle2_y(bullets_y[20 - 1:10]),
		.circle2_radius(BulletHalf),
		.collide(enemy_hits[1])
	);
	CirclesCollision circles_collision_inst_enemy_bullet_2(
		.valid(enemy_valid & bullets_valid[2]),
		.circle1_x(enemy_x),
		.circle1_y(enemy_y),
		.circle1_radius(EnemyHalf),
		.circle2_x(bullets_x[30 - 1:20]),
		.circle2_y(bullets_y[30 - 1:20]),
		.circle2_radius(BulletHalf),
		.collide(enemy_hits[2])
	);
	CirclesCollision circles_collision_inst_enemy_bullet_3(
		.valid(enemy_valid & bullets_valid[3]),
		.circle1_x(enemy_x),
		.circle1_y(enemy_y),
		.circle1_radius(EnemyHalf),
		.circle2_x(bullets_x[40 - 1:30]),
		.circle2_y(bullets_y[40 - 1:30]),
		.circle2_radius(BulletHalf),
		.collide(enemy_hits[3])
	);
	CirclesCollision circles_collision_inst_enemy_bullet_4(
		.valid(enemy_valid & bullets_valid[4]),
		.circle1_x(enemy_x),
		.circle1_y(enemy_y),
		.circle1_radius(EnemyHalf),
		.circle2_x(bullets_x[50 - 1:40]),
		.circle2_y(bullets_y[50 - 1:40]),
		.circle2_radius(BulletHalf),
		.collide(enemy_hits[4])
	);
	CirclesCollision circles_collision_inst_enemy_bullet_5(
		.valid(enemy_valid & bullets_valid[5]),
		.circle1_x(enemy_x),
		.circle1_y(enemy_y),
		.circle1_radius(EnemyHalf),
		.circle2_x(bullets_x[60 - 1:50]),
		.circle2_y(bullets_y[60 - 1:50]),
		.circle2_radius(BulletHalf),
		.collide(enemy_hits[5])
	);
    assign enemy_hit = (|(enemy_hits));
endmodule

/*
 * Handle One Bullet to Many Enemies Collision
 * So we can erase bullet easily
 */
module BulletEnemiesCollision(
    input bullet_valid,
    input [10 - 1:0] bullet_x,
    input [10 - 1:0] bullet_y,
    input [`MaxEnemy - 1:0] enemies_valid,
    input [`MaxEnemy * 10 - 1:0] enemies_x,
    input [`MaxEnemy * 10 - 1:0] enemies_y,
    output bullet_hit
);
    localparam EnemyHalf = 10'd20;
    localparam BulletHalf = 10'd5;

    wire [`MaxEnemy - 1:0] bullet_hits;
	CirclesCollision circles_collision_inst_bullet_enemy_0(
		.valid(bullet_valid & enemies_valid[0]),
		.circle1_x(bullet_x),
		.circle1_y(bullet_y),
		.circle1_radius(BulletHalf),
		.circle2_x(enemies_x[10 - 1:0]),
		.circle2_y(enemies_y[10 - 1:0]),
		.circle2_radius(EnemyHalf),
		.collide(bullet_hits[0])
	);
	CirclesCollision circles_collision_inst_bullet_enemy_1(
		.valid(bullet_valid & enemies_valid[1]),
		.circle1_x(bullet_x),
		.circle1_y(bullet_y),
		.circle1_radius(BulletHalf),
		.circle2_x(enemies_x[20 - 1:10]),
		.circle2_y(enemies_y[20 - 1:10]),
		.circle2_radius(EnemyHalf),
		.collide(bullet_hits[1])
	);
	CirclesCollision circles_collision_inst_bullet_enemy_2(
		.valid(bullet_valid & enemies_valid[2]),
		.circle1_x(bullet_x),
		.circle1_y(bullet_y),
		.circle1_radius(BulletHalf),
		.circle2_x(enemies_x[30 - 1:20]),
		.circle2_y(enemies_y[30 - 1:20]),
		.circle2_radius(EnemyHalf),
		.collide(bullet_hits[2])
	);
	CirclesCollision circles_collision_inst_bullet_enemy_3(
		.valid(bullet_valid & enemies_valid[3]),
		.circle1_x(bullet_x),
		.circle1_y(bullet_y),
		.circle1_radius(BulletHalf),
		.circle2_x(enemies_x[40 - 1:30]),
		.circle2_y(enemies_y[40 - 1:30]),
		.circle2_radius(EnemyHalf),
		.collide(bullet_hits[3])
	);
	CirclesCollision circles_collision_inst_bullet_enemy_4(
		.valid(bullet_valid & enemies_valid[4]),
		.circle1_x(bullet_x),
		.circle1_y(bullet_y),
		.circle1_radius(BulletHalf),
		.circle2_x(enemies_x[50 - 1:40]),
		.circle2_y(enemies_y[50 - 1:40]),
		.circle2_radius(EnemyHalf),
		.collide(bullet_hits[4])
	);
	assign bullet_hit = (|(bullet_hits));
endmodule

/*
 * Handle All Bullets, Enemies Collision
 */
module EnemiesBulletsCollision(
    input [`MaxEnemy - 1:0] enemies_valid,
    input [`MaxEnemy * 10 - 1:0] enemies_x,
    input [`MaxEnemy * 10 - 1:0] enemies_y,
    input [`MaxBullet - 1:0] bullets_valid,
    input [`MaxBullet * 10 - 1:0] bullets_x,
    input [`MaxBullet * 10 - 1:0] bullets_y,
    output [`MaxEnemy - 1:0] enemies_hit,
    output [`MaxBullet - 1:0] bullets_hit
);
    // declare constants
    localparam EnemyHalf = 10'd20;
    localparam BulletHalf = 10'd5;
    
    // Connect EnemyBulletsCollision Modules
	EnemyBulletsCollision enemy_bullets_collision_inst_0(
		.enemy_valid(enemies_valid[0]),
		.enemy_x(enemies_x[10 - 1:0]),
		.enemy_y(enemies_y[10 - 1:0]),
		.bullets_valid(bullets_valid),
		.bullets_x(bullets_x),
		.bullets_y(bullets_y),
		.enemy_hit(enemies_hit[0])
	);
	EnemyBulletsCollision enemy_bullets_collision_inst_1(
		.enemy_valid(enemies_valid[1]),
		.enemy_x(enemies_x[20 - 1:10]),
		.enemy_y(enemies_y[20 - 1:10]),
		.bullets_valid(bullets_valid),
		.bullets_x(bullets_x),
		.bullets_y(bullets_y),
		.enemy_hit(enemies_hit[1])
	);
	EnemyBulletsCollision enemy_bullets_collision_inst_2(
		.enemy_valid(enemies_valid[2]),
		.enemy_x(enemies_x[30 - 1:20]),
		.enemy_y(enemies_y[30 - 1:20]),
		.bullets_valid(bullets_valid),
		.bullets_x(bullets_x),
		.bullets_y(bullets_y),
		.enemy_hit(enemies_hit[2])
	);
	EnemyBulletsCollision enemy_bullets_collision_inst_3(
		.enemy_valid(enemies_valid[3]),
		.enemy_x(enemies_x[40 - 1:30]),
		.enemy_y(enemies_y[40 - 1:30]),
		.bullets_valid(bullets_valid),
		.bullets_x(bullets_x),
		.bullets_y(bullets_y),
		.enemy_hit(enemies_hit[3])
	);
	EnemyBulletsCollision enemy_bullets_collision_inst_4(
		.enemy_valid(enemies_valid[4]),
		.enemy_x(enemies_x[50 - 1:40]),
		.enemy_y(enemies_y[50 - 1:40]),
		.bullets_valid(bullets_valid),
		.bullets_x(bullets_x),
		.bullets_y(bullets_y),
		.enemy_hit(enemies_hit[4])
	);
    // Connect BulletEnemies Modules
	BulletEnemiesCollision bullet_enemies_collision_inst_0(
		.bullet_valid(bullets_valid[0]),
		.bullet_x(bullets_x[10 - 1:0]),
		.bullet_y(bullets_y[10 - 1:0]),
		.enemies_valid(enemies_valid),
		.enemies_x(enemies_x),
		.enemies_y(enemies_y),
		.bullet_hit(bullets_hit[0])
	);
	BulletEnemiesCollision bullet_enemies_collision_inst_1(
		.bullet_valid(bullets_valid[1]),
		.bullet_x(bullets_x[20 - 1:10]),
		.bullet_y(bullets_y[20 - 1:10]),
		.enemies_valid(enemies_valid),
		.enemies_x(enemies_x),
		.enemies_y(enemies_y),
		.bullet_hit(bullets_hit[1])
	);
	BulletEnemiesCollision bullet_enemies_collision_inst_2(
		.bullet_valid(bullets_valid[2]),
		.bullet_x(bullets_x[30 - 1:20]),
		.bullet_y(bullets_y[30 - 1:20]),
		.enemies_valid(enemies_valid),
		.enemies_x(enemies_x),
		.enemies_y(enemies_y),
		.bullet_hit(bullets_hit[2])
	);
	BulletEnemiesCollision bullet_enemies_collision_inst_3(
		.bullet_valid(bullets_valid[3]),
		.bullet_x(bullets_x[40 - 1:30]),
		.bullet_y(bullets_y[40 - 1:30]),
		.enemies_valid(enemies_valid),
		.enemies_x(enemies_x),
		.enemies_y(enemies_y),
		.bullet_hit(bullets_hit[3])
	);
	BulletEnemiesCollision bullet_enemies_collision_inst_4(
		.bullet_valid(bullets_valid[4]),
		.bullet_x(bullets_x[50 - 1:40]),
		.bullet_y(bullets_y[50 - 1:40]),
		.enemies_valid(enemies_valid),
		.enemies_x(enemies_x),
		.enemies_y(enemies_y),
		.bullet_hit(bullets_hit[4])
	);
	BulletEnemiesCollision bullet_enemies_collision_inst_5(
		.bullet_valid(bullets_valid[5]),
		.bullet_x(bullets_x[60 - 1:50]),
		.bullet_y(bullets_y[60 - 1:50]),
		.enemies_valid(enemies_valid),
		.enemies_x(enemies_x),
		.enemies_y(enemies_y),
		.bullet_hit(bullets_hit[5])
	);
endmodule

module EnemiesWallCollision(
	input [`MaxEnemy - 1:0] enemies_valid,
	input [`MaxEnemy * 10 - 1:0] enemies_x,
	output collide
);
	localparam WallX = 10'd55;
	localparam EnemyHalf = 10'd20;

	wire [`MaxEnemy - 1:0] wall_collide;
	assign wall_collide[0] = enemies_valid[0] && ((enemies_x[10 - 1:0] - EnemyHalf) <= WallX);
	assign wall_collide[1] = enemies_valid[1] && ((enemies_x[20 - 1:10] - EnemyHalf) <= WallX);
	assign wall_collide[2] = enemies_valid[2] && ((enemies_x[30 - 1:20] - EnemyHalf) <= WallX);
	assign wall_collide[3] = enemies_valid[3] && ((enemies_x[40 - 1:30] - EnemyHalf) <= WallX);
	assign wall_collide[4] = enemies_valid[4] && ((enemies_x[50 - 1:40] - EnemyHalf) <= WallX);

	assign collide = (|(wall_collide));
endmodule
