`timescale 1ns/1ps
`define CYC 4

module random_number_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;
    
    wire [2 - 1:0] out2;
    wire [3 - 1:0] out3;
    wire [4 - 1:0] out4;
    wire [5 - 1:0] out5;
    wire [6 - 1:0] out6;
    wire [7 - 1:0] out7;
    wire [8 - 1:0] out8;
    wire [9 - 1:0] out9;
    wire [10 - 1:0] out10;
    wire [11 - 1:0] out11;
    wire [12 - 1:0] out12;
    wire [13 - 1:0] out13;
    wire [14 - 1:0] out14;
    wire [15 - 1:0] out15;
    wire [16 - 1:0] out16;
    wire [17 - 1:0] out17;
    wire [18 - 1:0] out18;
    wire [19 - 1:0] out19;
    wire [20 - 1:0] out20;
    wire [21 - 1:0] out21;
    wire [22 - 1:0] out22;
    wire [23 - 1:0] out23;
    wire [24 - 1:0] out24;
    wire [25 - 1:0] out25;
    wire [26 - 1:0] out26;
    wire [27 - 1:0] out27;
    wire [28 - 1:0] out28;
    wire [29 - 1:0] out29;
    wire [30 - 1:0] out30;
    wire [31 - 1:0] out31;
    wire [32 - 1:0] out32;

    RandomInteger #(2) random_integer_inst_2(clk, reset, out2);
    RandomInteger #(3) random_integer_inst_3(clk, reset, out3);
    RandomInteger #(4) random_integer_inst_4(clk, reset, out4);
    RandomInteger #(5) random_integer_inst_5(clk, reset, out5);
    RandomInteger #(6) random_integer_inst_6(clk, reset, out6);
    RandomInteger #(7) random_integer_inst_7(clk, reset, out7);
    RandomInteger #(8) random_integer_inst_8(clk, reset, out8);
    RandomInteger #(9) random_integer_inst_9(clk, reset, out9);
    RandomInteger #(10) random_integer_inst_10(clk, reset, out10);
    RandomInteger #(11) random_integer_inst_11(clk, reset, out11);
    RandomInteger #(12) random_integer_inst_12(clk, reset, out12);
    RandomInteger #(13) random_integer_inst_13(clk, reset, out13);
    RandomInteger #(14) random_integer_inst_14(clk, reset, out14);
    RandomInteger #(15) random_integer_inst_15(clk, reset, out15);
    RandomInteger #(16) random_integer_inst_16(clk, reset, out16);
    RandomInteger #(17) random_integer_inst_17(clk, reset, out17);
    RandomInteger #(18) random_integer_inst_18(clk, reset, out18);
    RandomInteger #(19) random_integer_inst_19(clk, reset, out19);
    RandomInteger #(20) random_integer_inst_20(clk, reset, out20);
    RandomInteger #(21) random_integer_inst_21(clk, reset, out21);
    RandomInteger #(22) random_integer_inst_22(clk, reset, out22);
    RandomInteger #(23) random_integer_inst_23(clk, reset, out23);
    RandomInteger #(24) random_integer_inst_24(clk, reset, out24);
    RandomInteger #(25) random_integer_inst_25(clk, reset, out25);
    RandomInteger #(26) random_integer_inst_26(clk, reset, out26);
    RandomInteger #(27) random_integer_inst_27(clk, reset, out27);
    RandomInteger #(28) random_integer_inst_28(clk, reset, out28);
    RandomInteger #(29) random_integer_inst_29(clk, reset, out29);
    RandomInteger #(30) random_integer_inst_30(clk, reset, out30);
    RandomInteger #(31) random_integer_inst_31(clk, reset, out31);
    RandomInteger #(32) random_integer_inst_32(clk, reset, out32);

    always #(`CYC / 2) clk <= ~clk;

    initial begin
        @(negedge clk)
        reset = 1'b1;
        @(negedge clk)
        reset = 1'b0;

        #(`CYC * 1000);
        $finish;
    end
endmodule
