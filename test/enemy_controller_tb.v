`timescale 1ns/1ps

`define CYC 4

module enemy_controller_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;
    reg [15 - 1:0] hits;
    wire [15 - 1:0] alives;
    wire [15 - 1:0] types;
    wire [10 - 1:0] position_x [0:15 - 1];
    wire [10 - 1:0] position_y [0:15 - 1];
    wire [6 - 1:0] died_counter;

    always #(`CYC / 2) clk = ~clk;

    EnemyController enemy_controller_inst(
        .clk(clk),
        .reset(reset),
        .hits(hits),
        .damage(5'd1),
        .alives(alives),
        .types(types),
        .positions_x(
            {position_x[14], position_x[13], position_x[12], position_x[11], position_x[10], position_x[9], position_x[8], position_x[7], position_x[6], position_x[5], position_x[4], position_x[3], position_x[2], position_x[1], position_x[0]}
        ),
        .positions_y(
            {position_y[14], position_y[13], position_y[12], position_y[11], position_y[10], position_y[9], position_y[8], position_y[7], position_y[6], position_y[5], position_y[4], position_y[3], position_y[2], position_y[1], position_y[0]}
        ),
        .died_counter(died_counter)
    );

    initial begin
        @(negedge clk)
        reset = 1'b1;
        @(negedge clk)
        reset = 1'b0;

        #(`CYC * 150);
        @(negedge clk)
        hits = 15'b000_0000_0000_0001;
        @(negedge clk)
        hits = 15'b000_0000_0000_0000;

        #(`CYC * 10);
        @(negedge clk)
        hits = 15'b000_0000_0000_0001;
        @(negedge clk)
        hits = 15'b000_0000_0000_0000;

        #(`CYC * 1000)
        #1 $finish;
    end
endmodule
