
`timescale 1ns/1ps
`define CYC 4

module vga_display_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;

    always #(`CYC / 4) clk = ~clk;

    reg play = 1'b0;
    reg cheat = 1'b0;
    reg [10 - 1:0] distance = 10'd100;
    wire hsync;
    wire vsync;
    wire [4 - 1:0] vgaRed;
    wire [4 - 1:0] vgaGreen;
    wire [4 - 1:0] vgaBlue;

    VGADisplay vga_display_inst(
        .clk(clk),
        .reset(reset),
        .play(play),
        .cheat(cheat),
        .distance(distance),
        .hsync(hsync),
        .vsync(vsync),
        .vgaRed(vgaRed),
        .vgaGreen(vgaGreen),
        .vgaBlue(vgaBlue)
    );

    initial begin
        @(negedge clk)
        reset = 1'b1;
        @(negedge clk)
        reset = 1'b0;

        @(negedge clk)
        play = 1'b1;
        @(negedge clk)
        play = 1'b0;

        #(`CYC * 100000); $finish;  
    end
endmodule
