`timescale 1ns/1ps
`define CYC 4
`define MaxEnemy 15
`define MaxBullet 10

module EnemiesBulletsCollision_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;
    reg [10 - 1:0] h_cnt = 10'b0;
    reg [10 - 1:0] v_cnt = 10'b0;

    always #(`CYC / 2) clk = ~clk;

    wire [`MaxEnemy - 1:0] enemies_alive;
    wire [`MaxEnemy * 10 - 1:0] enemies_x;
    wire [`MaxEnemy * 10 - 1:0] enemies_y;
    wire [`MaxEnemy - 1:0] enemies_hit;
    wire [`MaxBullet -  1:0] bullets_exist;
    wire [`MaxBullet * 10 - 1:0] bullets_x;
    wire [`MaxBullet * 10 - 1:0] bullets_y;
    wire [`MaxBullet - 1:0] bullets_hit;
    // Handle Collision
    EnemiesBulletsCollision enemies_bullets_collision_inst(
        .enemies_valid(enemies_alive),
        .enemies_x(enemies_x),
        .enemies_y(enemies_y),
        .bullets_valid(bullets_exist),
        .bullets_x(bullets_x),
        .bullets_y(bullets_y),
        .enemies_hit(enemies_hit), // tell us which enemy is hit
        .bullets_hit(bullets_hit) // tell us which bullet hit the enemy
    );
    BulletsDisplayController bullets_display_controller_inst(
        .clk(clk),
        .reset(reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .distance(10'd100),
        .exist(is_bullet), // whether bullet should show on the screen at (h_cnt, v_cnt) now or not
        .address(bullet_address), // for reading the image address
        .exists(bullets_exist), // tell us which bullet exist
        .positions_x(bullets_x),
        .positions_y(bulelts_y)
    );

    // enemies
    wire is_enemy;
    wire enemy_type;
    EnemiesDisplayController enemies_display_controller_inst(
        .clk(clk),
        .reset(reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .hits(enemies_hit),
        .damage(5'd1),
        .valid(is_enemy),
        .type(enemy_type),
        .address(enemy_address),
        .alives(enemies_alive),
        .positions_x(enemies_x),
        .positions_y(enemies_y),
        .died_counter(enemy_died_counter)
    );

    initial begin
        @(posedge clk)
        reset = 1'b1;
        @(posedge clk)
        reset = 1'b0;

        @(posedge clk)
        repeat(100) begin
            v_cnt = 10'b0;
            repeat(480) begin
                h_cnt = 10'b0;
                repeat(640) begin
                    #(`CYC);
                    h_cnt = h_cnt + 1'b1;
                    if (h_cnt & {10{1'b1}})
                        hits = 15'b11111_11111_11111;
                    else
                        hits = 15'b0;
                end
                v_cnt = v_cnt + 1'b1;
            end
        end
        #1 $finish;
    end

endmodule
