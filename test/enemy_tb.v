
`timescale 1ns/1ps
`define CYC 4

module enemy_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;

    always #(`CYC / 2) clk = ~clk;

    reg hit;
    reg [5 - 1:0] damage;
    reg spawn;
    reg spawn_type;
    reg [9 - 1:0] spawn_y;
    wire alive;
    wire type;
    wire [10 - 1:0] position_x;
    wire [10 - 1:0] position_y;
    wire died;

    Enemy enemy_inst(
        .clk(clk),
        .reset(reset),
        .hit(hit),
        .damage(damage),
        .spawn(spawn),
        .spawn_type(spawn_type),
        .spawn_y(spawn_y),
        .alive(alive),
        .type(type),
        .position_x(position_x),
        .position_y(position_y),
        .died(died)
    );

    initial begin
        @(negedge clk)
        reset = 1'b1;
        spawn = 1'b0;
        spawn_type = 1'b0;
        spawn_y = 9'd0;
        hit = 1'b0;
        damage = 5'd1;
        @(negedge clk)
        reset = 1'b0;

        @(negedge clk)
        spawn = 1'b1;
        spawn_type = 1'b0;
        spawn_y = 9'd20;

        @(negedge clk)
        spawn = 1'b0;

        @(negedge clk)
        hit = 1'b1;
        damage = 5'd1;
        @(negedge clk)
        hit = 1'b0;
        
        #(`CYC * 10);
        @(negedge clk)
        hit = 1'b1;
        damage = 5'd1;
        @(negedge clk)
        hit = 1'b0;
        
        #(`CYC * 6);
        @(negedge clk)
        hit = 1'b1;
        damage = 5'd1;
        @(negedge clk)
        hit = 1'b0;

        #(`CYC * 100);
        $finish;
    end
endmodule
