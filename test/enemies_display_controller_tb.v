
`timescale 1ns/1ps
`define CYC 4

module enemies_display_controller_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;

    always #(`CYC / 2) clk = ~clk;

    reg [10 - 1:0] h_cnt = 10'b0;
    reg [10 - 1:0] v_cnt = 10'b0;
    reg [15 - 1:0] hits = 15'b0;
    wire is_enemy;
    wire enemy_type;
    wire [12 - 1:0] enemy_address;
    wire [15 - 1:0] enemies_alive;
    wire [15 * 10 - 1:0] enemies_x;
    wire [15 * 10 - 1:0] enemies_y;
    wire [6 - 1:0] enemy_died_counter;
    

    EnemiesDisplayController enemies_display_controller_inst(
        .clk(clk),
        .reset(reset),
        .h_cnt(h_cnt),
        .v_cnt(v_cnt),
        .hits(hits),
        .damage(5'd1),
        .valid(is_enemy),
        .type(enemy_type),
        .address(enemy_address),
        .alives(enemies_alive),
        .positions_x(enemies_x),
        .positions_y(enemies_y),
        .died_counter(enemy_died_counter)
    );

    initial begin
        @(posedge clk)
        reset = 1'b1;
        @(posedge clk)
        reset = 1'b0;

        @(posedge clk)
        repeat(100) begin
            v_cnt = 10'b0;
            repeat(480) begin
                h_cnt = 10'b0;
                repeat(640) begin
                    #(`CYC);
                    h_cnt = h_cnt + 1'b1;
                    if (h_cnt & {10{1'b1}})
                        hits = 15'b11111_11111_11111;
                    else
                        hits = 15'b0;
                end
                v_cnt = v_cnt + 1'b1;
            end
        end
        #1 $finish;
    end
endmodule
