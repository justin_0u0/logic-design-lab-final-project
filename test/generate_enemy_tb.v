`timescale 1ns/1ps
`define CYC 4

module generate_enemy_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;
    wire spawn;
    wire spawn_type;
    wire [9 - 1:0] spawn_y;

    GenerateEnemy generate_enemy_inst(clk, reset, spawn, spawn_type, spawn_y);

    always #(`CYC / 2) clk <= ~clk;

    initial begin
        @(negedge clk)
        reset = 1'b1;
        @(negedge clk)
        reset = 1'b0;

        #(`CYC * 1000); $finish;
    end
endmodule
