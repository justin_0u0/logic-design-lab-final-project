`timescale 1ns/1ps

`define CYC 4

module bullet_controller_tb;
    reg clk = 1'b1;
    reg reset = 1'b0;
    wire [10 - 1:0] exists;
    wire [10 - 1:0] position_x [0:10 - 1];
    wire [10 - 1:0] position_y [0:10 - 1];

    always #(`CYC / 2) clk = ~clk;

    BulletController bullet_controller_inst_0(
        .clk(clk),
        .reset(reset),
        .exists(exists),
        .position_x(
            {position_x[0], position_x[1], position_x[2], position_x[3], position_x[4], position_x[5], position_x[6], position_x[7], position_x[8], position_x[9]}
        ),
        .position_y(
            {position_y[0], position_y[1], position_y[2], position_y[3], position_y[4], position_y[5], position_y[6], position_y[7], position_y[8], position_y[9]}
        )
    );

    initial begin
        @(negedge clk)
        reset = 1'b1;
        @(negedge clk)
        reset = 1'b0;

        #(`CYC * 1000)
        #1 $finish;
    end
endmodule