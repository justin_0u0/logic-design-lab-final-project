module game_controller_tb(
    input clk,
    input btnC,
    input echo, // A17
    output trig, // A15
    output [4 - 1:0] an,
    output [7 - 1:0] seg,
    output dp,
    output hsync,
    output vsync,
    output [4 - 1:0] vgaRed,
    output [4 - 1:0] vgaGreen,
    output [4 - 1:0] vgaBlue,
    output [2 - 1:0] led
);

    GameController game_controller_inst(
        .clk(clk),
        .btnC(btnC),
        .echo(echo),
        .trig(trig),
        .an(an),
        .seg(seg),
        .dp(dp),
        .hsync(hsync),
        .vsync(vsync),
        .vgaRed(vgaRed),
        .vgaGreen(vgaGreen),
        .vgaBlue(vgaBlue),
        .led(led)
    );

endmodule
