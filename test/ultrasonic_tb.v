module TopModuleTest(
    input clk,
    input btnC,
    input echo,
    output trig,
    output [4 - 1:0] an,
    output [7 - 1:0] seg,
    output dp,
    output [1:0] LED
);

    wire btnC_d, reset;
    wire [20 - 1:0] distance;

    assign LED[0] = echo;
    assign LED[1] = (distance < 20'd100) ? 1'b1 : 1'b0;

    Debounce debounce_inst_0(clk, btnC, btnC_d);
    OnePulse onepulse_inst_0(clk, btnC_d, reset);

    Trig_1Hz trig_1hz_inst_0(clk, reset, trig);

    CountDistance count_distance(
        .clk(clk),
        .reset(reset),
        .echo(echo),
        .distance(distance)
    );

    SevenSegment seven_segment(
        .clk(clk),
        .reset(reset),
        .values(distance),
        .an(an),
        .seg(seg),
        .dp(dp)
    );

endmodule