`define CYC 4

module circlescollision_tb;
    reg clk = 1'b1;
    reg valid = 1'b0;
    reg [10 - 1:0] circle1_x;
    reg [10 - 1:0] circle1_y;
    reg [10 - 1:0] circle1_radius = 10'd20;
    reg [10 - 1:0] circle2_x;
    reg [10 - 1:0] circle2_y;
    reg [10 - 1:0] circle2_radius = 10'd5;
    wire collide;

    CirclesCollision circlescollision_inst_0(
        .valid(valid),
        .circle1_x(circle1_x),
        .circle1_y(circle2_y),
        .circle1_radius(circle1_radius),
        .circle2_x(circle2_x),
        .circle2_y(circle2_y),
        .circle2_radius(circle2_radius),
        .collide(collide)
    );

    always #(`CYC / 2) clk = ~clk;

    initial begin
        circle1_x = 10'd640;
        circle1_y = 10'd240;
        circle2_x = 10'd55;
        circle2_y = 10'd230;
        repeat(100) begin
            #(`CYC)
            circle1_x = circle1_x - 10'd1;
            circle2_x = circle2_x + 10'd5;
        end
    end

endmodule
