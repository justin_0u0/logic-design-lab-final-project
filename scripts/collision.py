
# EnemyBulletsCollision
for i in range(10):
    print(f'\tCirclesCollision circles_collision_inst_enemy_bullet_{i}(')
    print(f'\t\t.valid(enemy_valid & bullets_valid[{i}]),')
    print('\t\t.circle1_x(enemy_x),')
    print('\t\t.circle1_y(enemy_y),')
    print('\t\t.circle1_radius(EnemyHalf),')
    print(f'\t\t.circle2_x(bullets_x[{(i + 1) * 10} - 1:{i * 10}]),')
    print(f'\t\t.circle2_y(bullets_y[{(i + 1) * 10} - 1:{i * 10}]),')
    print('\t\t.circle2_radius(BulletHalf),')
    print(f'\t\t.collide(enemy_hits[{i}])')
    print('\t);')

for i in range(15):
    print(f'\tCirclesCollision circles_collision_inst_bullet_enemy_{i}(')
    print(f'\t\t.valid(bullet_valid & enemies_valid[{i}]),')
    print('\t\t.circle1_x(bullet_x),')
    print('\t\t.circle1_y(bullet_y),')
    print('\t\t.circle1_radius(BulletHalf),')
    print(f'\t\t.circle2_x(enemies_x[{(i + 1) * 10} - 1:{i * 10}]),')
    print(f'\t\t.circle2_y(enemies_y[{(i + 1) * 10} - 1:{i * 10}]),')
    print('\t\t.circle2_radius(EnemyHalf),')
    print(f'\t\t.collide(bullet_hits[{i}])')
    print('\t);')

# EnemiesBulletsCollision
for i in range(15):
    print(f'\tEnemyBulletsCollision enemy_bullets_collision_inst_{i}(')
    print(f'\t\t.enemy_valid(enemies_valid[{i}]),')
    print(f'\t\t.enemy_x(enemies_x[{(i + 1) * 10} - 1:{i * 10}]),')
    print(f'\t\t.enemy_y(enemies_y[{(i + 1) * 10} - 1:{i * 10}]),')
    print('\t\t.bullets_valid(bullets_valid),')
    print('\t\t.bullets_x(bullets_x),')
    print('\t\t.bullets_y(bullets_y),')
    print(f'\t\t.enemy_hit(enemies_hit[{i}])')
    print('\t);')
for i in range(10):
    print(f'\tBulletEnemiesCollision bullet_enemies_collision_inst_{i}(')
    print(f'\t\t.bullet_valid(bullets_valid[{i}]),')
    print(f'\t\t.bullet_x(bullets_x[{(i + 1) * 10} - 1:{i * 10}]),')
    print(f'\t\t.bullet_y(bullets_y[{(i + 1) * 10} - 1:{i * 10}]),')
    print('\t\t.enemies_valid(enemies_valid),')
    print('\t\t.enemies_x(enemies_x),')
    print('\t\t.enemies_y(enemies_y),')
    print(f'\t\t.bullet_hit(bullets_hit[{i}])')
    print('\t);')

