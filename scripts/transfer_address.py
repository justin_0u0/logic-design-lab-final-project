import sys
image = sys.argv[1]

print(f'assign {image}[0]', end='');
for i in range (1, 2500):
    print(f', {image}[{i}]', end='')
    if ((i + 1) % 100) == 0:
        print()
print("} =")