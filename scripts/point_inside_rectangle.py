
# bullets
for i in range(10):
    print(f'\tPointInsideRectangle point_inside_rectangle_inst_bullet_{i}(')
    print(f'\t\t.valid(bullets[{i}]),')
    print('\t\t.point_x(h_cnt),')
    print('\t\t.point_y(v_cnt),')
    print(f'\t\t.rectangle_x(bullets_x[{i}]),')
    print(f'\t\t.rectangle_y(bullets_y[{i}]),')
    print('\t\t.half_rectangle_size(BulletHalf),')
    print(f'\t\t.inside(is_bullets[{i}])')
    print('\t);')

# enemies
for i in range(15):
    print(f'\tPointInsideRectangle point_inside_rectangle_inst_enemy_{i}(')
    print(f'\t\t.valid(enemies[{i}]),')
    print('\t\t.point_x(h_cnt),')
    print('\t\t.point_y(v_cnt),')
    print(f'\t\t.rectangle_x(enemies_x[{i}]),')
    print(f'\t\t.rectangle_y(enemies_y[{i}]),')
    print('\t\t.half_rectangle_size(EnemyHalf),')
    print(f'\t\t.inside(is_enemies[{i}])')
    print('\t);')

