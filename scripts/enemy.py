
# EnemyController
for i in range(15):
    print(f'\tEnemy enemy_inst_{i}(')
    print('\t\t.clk(clk),')
    print('\t\t.reset(reset),')
    print(f'\t\t.hit(hits[{i}]),')
    print(f'\t\t.damage(damage),')
    print(f'\t\t.spawn(spawns[{i}]),')
    print('\t\t.spawn_type(spawn_type),')
    print('\t\t.spawn_y(spawn_y),')
    print(f'\t\t.alive(alives[{i}]),')
    print(f'\t\t.type(types[{i}]),')
    print(f'\t\t.position_x(positions_x[{(i + 1) * 10} - 1:{i * 10}]),')
    print(f'\t\t.position_y(positions_y[{(i + 1) * 10} - 1:{i * 10}]),')
    print(f'\t\t.died(dieds[{i}])')
    print('\t);')

