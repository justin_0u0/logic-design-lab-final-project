
import sys
filename = sys.argv[1]
linebreak = int(sys.argv[2])

counter = 0
with open(filename, 'r') as f:
    for line in f:
        if line[0] == 'm':
            continue
        line = line.rstrip('\n')
        line = line.replace(';', '')
        print(f'12\'h{line}', end=' ')
        counter += 1
        if counter == linebreak:
            counter = 0
            print()

